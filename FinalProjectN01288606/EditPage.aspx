﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditPage.aspx.cs" Inherits="FinalProjectN01288606.EditPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h3 runat="server" id="pagetitle_div">Edit Page</h3>


    <asp:SqlDataSource ID="edit_page_select" runat="server" ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
        </asp:SqlDataSource>

        <div class="inputrow">
        <label>Page Title:</label>
        <asp:TextBox ID="pagetitle" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
           ID="pagetitle_validator" ControlToValidate="pagetitle"
            ErrorMessage="Enter a title">
        </asp:RequiredFieldValidator>
    </div>

    <div class="inputrow">
        <label>Page Content:</label>
        <asp:TextBox ID="pagecontent" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
           ID="pagecontent_validator" ControlToValidate="pagecontent"
            ErrorMessage="Enter some content">
        </asp:RequiredFieldValidator>
    </div>

    <div class="inputrow">
        <label>Page Author:</label>
        <asp:TextBox ID="pageauthor" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
           ID="RequiredFieldValidator1" ControlToValidate="pageauthor"
            ErrorMessage="Enter an Author">
        </asp:RequiredFieldValidator>
    </div>

    <ASP:Button Text="Edit Page" runat="server" OnClick="Edit_Page"/>

    <div runat="server" id="debug" class="querybox"></div>

</asp:Content>
