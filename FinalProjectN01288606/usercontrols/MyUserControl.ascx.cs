﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace FinalProjectN01288606.Usercontrols
{
    public partial class MyUserControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //use the sqldata source to look at every page
            /*
            if(Page.IsPostBack)
            {
                return;
            }*/

            string query = "select * from pages";
            pages_select.SelectCommand = query;
           
            DataView mytbl = (DataView)pages_select.Select(DataSourceSelectArguments.Empty);

        //write a function which looks at the result set row by row
        string MenuItems = "";

            foreach (DataRow row in mytbl.ToTable().Rows)
            {
                
                string title = row["pagetitle"].ToString();
                string pageid = row["pageid"].ToString();
                //<li><a href="Page.aspx?pageid=1">Mushrooms</a></li>
                

                MenuItems += "<li><a href=\"ViewPage.aspx?pageid=" + pageid + "\">" + title + "</a></li>";
            }
            page_container.InnerHtml = MenuItems;
        }
        
    }
}