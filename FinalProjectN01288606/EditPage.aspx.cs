﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace FinalProjectN01288606
{
    public partial class EditPage : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Edit_Page()
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DataRowView pagerow = getPageInfo(pageid);
            if (pagerow == null)
            {
                pagetitle_div.InnerHtml = "No Page Found.";
                return;
            }
            pagetitle.Text = pagerow["pagetitle"].ToString();
            pagecontent.Text = pagerow["pagecontent"].ToString();
            pageauthor.Text = pagerow["pageauthor"].ToString();
            //I borrowed this section from the Crud Example
        }
        protected void Edit_Page(object sender, EventArgs e)
        {
            string title = pagetitle.Text;
            string content = pagecontent.Text;
            string author = pageauthor.Text;

            string editquery = "Update Pages set pagetitle= '" + title + "'," +
                " pagecontent='" + content + "'," + " pageauthor= '" + author + "'" + " where pageid=" + pageid;
            //debug.InnerHtml = editquery;

            edit_page_select.UpdateCommand = editquery;
            edit_page_select.Update();
        }
        protected DataRowView getPageInfo(int id)
        {
            string query = "select * from pages where pageid=" +
                pageid.ToString();
            edit_page_select.SelectCommand = query;
            DataView pageview = (DataView)edit_page_select.Select
                (DataSourceSelectArguments.Empty);
            if(pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0];
            return pagerowview;
        }
        }

}