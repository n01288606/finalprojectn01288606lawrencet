﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace FinalProjectN01288606
{
    public partial class ManagePages : System.Web.UI.Page
    {
        private string basequery = "SELECT * FROM PAGES";

        protected void Page_Load(object sender, EventArgs e)
        {
            pages_select.SelectCommand = basequery;
            pages_list.DataSource = Pages_Manual_Bind(pages_select);
           // pages_query.InnerHtml = basequery;
            pages_list.DataBind();
        }

        protected DataView Pages_Manual_Bind(SqlDataSource src)//Render Loop not working right, where connected
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select
                (DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                //what im doing row by row of the result set is manipulating
                //the datatable to modify columns
                row["pagetitle"] =
                    "<a href=\"ViewPage.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row["pagetitle"]
                    + "</a>";

                // LOOKS like <a href="viewpage.aspx?pageid=1">Ninjas</a>
            }
            mytbl.Columns["pagetitle"].ColumnName = "Page Title";
            mytbl.Columns["pagecontent"].ColumnName = "Content";
            mytbl.Columns["pageauthor"].ColumnName = "Author";
            mytbl.Columns["publish_date"].ColumnName = "Date Published";
            mytbl.Columns.Remove("pageid");
            myview = mytbl.DefaultView;
            return myview;
        }
    }
}