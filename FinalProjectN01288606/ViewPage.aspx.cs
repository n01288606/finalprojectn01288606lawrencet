﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProjectN01288606
{
    public partial class ViewPage : System.Web.UI.Page
    {
        public string Pageid {
            get { return Request.QueryString["pageid"]; }
        }

        private string page_basequery =
            "SELECT pageid as 'pageid', pagetitle as 'Page Title', pagecontent as 'Page Content', pageauthor as 'Page Author', publish_date as'Date Published' from Pages";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Pageid == "" || Pageid == null) page_name.InnerHtml = "No page found.";
            else
            {
                page_basequery += " WHERE PAGEID =" + Pageid;
                view_page_select.SelectCommand = page_basequery;
                //page_query.InnerHtml = page_basequery;

                
                DataView pageview = (DataView)view_page_select.Select
                    (DataSourceSelectArguments.Empty);

                
                string pagetitle = pageview[0]["Page Title"].ToString();
                page_name.InnerHtml = pagetitle;
                string pagecontent = pageview[0]["Page Content"].ToString();
                page_content.InnerHtml = pagecontent;
                string pageauthor = pageview[0]["Page Author"].ToString();
                page_author.InnerHtml = pageauthor;
                string publish_date = pageview[0]["Date Published"].ToString();
                date_published.InnerHtml = publish_date;


                //page_content.InnerHTML = pagecontent;


            }
        }

        protected void DelPage(object sender, EventArgs e)
        {
            string delquery = "DELETE FROM PAGES WHERE pageid=" + Pageid;
           // del_debug.InnerHtml = delquery;
            del_page.DeleteCommand = delquery;
            del_page.Delete();


        }


    }

}