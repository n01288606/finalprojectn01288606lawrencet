﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewPage.aspx.cs" Inherits="FinalProjectN01288606.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h3 id="page_name" runat="server"></h3>
    <div class="view_page_con">
        <p id="page_content" runat="server"></p>
        <p id="page_author" runat="server"></p>
        <p id="date_published" runat="server"></p>
    </div>

    <asp:SqlDataSource
        runat="server"
        ID="del_page"
        ConnectionString="<%$ ConnectionStrings:final_sql_con %>"></asp:SqlDataSource>

    <div id="mybuttons">
        <asp:Button runat="server" ID="del_page_btn"
            OnClick="DelPage"
            OnClientClick="if(!confirm('Are you sure?')) return false;"
            Text="Delete" />
        <div id="del_debug" class="querybox" runat="server"></div>

        <a href="EditPage.aspx?Pageid=<%Response.Write(this.Pageid);%>">Edit</a>
        
        <!--<div id="page_query" runat="server" class="querybox">
        </div>-->
    </div>
    <asp:DataGrid ID="page_list" runat="server">
    </asp:DataGrid>



    <asp:SqlDataSource ID="view_page_select" runat="server" ConnectionString="<%$ ConnectionStrings:final_sql_con %>"></asp:SqlDataSource>
    <%--
        <asp:DataGrid runat="server" ID="view_page" DataSourceID="view_page_select" CellSpacing="3" width="100%" gridlines="Vertical">
        <HeaderStyle Font-Size="Large" BackColor="#00bfff" ForeColor="white" />
        <ItemStyle CssClass="tablerow" />

        </asp:DataGrid>
    --%>
</asp:Content>
