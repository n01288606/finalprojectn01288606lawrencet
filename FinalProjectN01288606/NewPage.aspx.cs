﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProjectN01288606
{
    public partial class NewPage : System.Web.UI.Page
    {
        private string addquery = "INSERT INTO PAGES" + "(pagetitle," + "pagecontent, pageauthor, publish_date) VALUES"; 

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void AddNewPage(object sender, EventArgs e)
        {
            string title = pagetitle.Text.ToString();
            string content = pagecontent.Text.ToString();
            string author = pageauthor.Text.ToString();
            DateTime pub_date = DateTime.Now;

            addquery += " ('" +
               title + "','" + content + "','" + author + "','" + pub_date +  "')";

           // debug.InnerHtml = addquery;

            
            insert_new_page.InsertCommand = addquery;

            insert_new_page.Insert();


        }
    }
}