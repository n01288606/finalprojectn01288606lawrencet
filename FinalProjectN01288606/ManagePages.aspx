﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManagePages.aspx.cs" Inherits="FinalProjectN01288606.ManagePages" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h2>My Pages</h2>

    <a href="NewPage.aspx">New Page</a>

    <asp:SqlDataSource ID="pages_select" runat="server"
        SelectCommand="select * from pages" ConnectionString="<%$ ConnectionStrings:final_sql_con %>"></asp:SqlDataSource>

    <div id="pages_query" class="querybox" runat="server">

    </div>

    <asp:DataGrid runat="server" ID="pages_list"  CellSpacing="3" width="100%" gridlines="Horizontal">
        <HeaderStyle Font-Size="Large" BackColor="#00bfff" ForeColor="white" />
        <ItemStyle CssClass="tablerow" />
    </asp:DataGrid>

</asp:Content>
