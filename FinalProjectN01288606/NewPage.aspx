﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewPage.aspx.cs" Inherits="FinalProjectN01288606.NewPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3>New Page</h3>

    <asp:SqlDataSource ID="insert_new_page" runat="server" ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
        </asp:SqlDataSource>

    <div class="inputrow">
        <label>Page Title:</label>
        <asp:TextBox runat="server" ID="pagetitle"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="pagetitle_validate" ControlToValidate="pagetitle"></asp:RequiredFieldValidator>
    </div>
    <div class="inputrow">
        <label>Page Content:</label>
        <asp:TextBox runat="server" ID="pagecontent"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="pagecontent_validate" ControlToValidate="pagecontent"></asp:RequiredFieldValidator>
    </div>
    <div class="inputrow">
        <label>Page Author:</label>
        <asp:TextBox runat="server" ID="pageauthor"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="pageauthor_validate" ControlToValidate="pageauthor"></asp:RequiredFieldValidator>
    </div>
    

    

      <ASP:Button Text="Add New Page" runat="server" OnClick="AddNewPage"/>

    <!--<div runat="server" id="debug" class="querybox"></div>-->

</asp:Content>
